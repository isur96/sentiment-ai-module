# Sentiment analysis

### Prerequisites
You need: `Python3`

Download dataset: `https://www.kaggle.com/kazanova/sentiment140`

Extract dataset to `./data/data.csv`

### Installing
`pip install -r requirements.txt`

### Environment Variables
Use `.env-template` to prepare your own `.env` file

### Prepare the dataset & model
`python learn.py`

### Run app
`flask run`

`gunicorn --bind 0.0.0.0:5000 wsgi`

Or use docker with `Dockerfile`
from pathlib import Path

ROOT_PATH = Path(__file__).parent.parent.__str__()
DATA_PATH = ROOT_PATH + "/data/data.csv"
MODEL_PATH = ROOT_PATH + "/data/model.pkl"
TOKENIZED_PATH = ROOT_PATH + "/data/tokenized.csv"

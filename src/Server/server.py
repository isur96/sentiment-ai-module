from flask import Flask, request, abort, jsonify
from .Predictor import Predictor


app = Flask(__name__)


@app.route("/")
def index():
    return "Hello, you probably want post to /sentiment with { text: string }"


@app.route("/sentiment", methods=['POST'])
def sentiment():
    if request.json['text'] is None: abort(400, 'Bad request')
    n = Predictor(request.json['text'])
    n.prediction()
    response = jsonify({
        'text': n.text,
        'sentiment': n.result
    })
    return response


import pickle
from src.Common.TextPrepare import predict
from src.paths import MODEL_PATH

c = None
t = None
try:
    with open(MODEL_PATH, "rb") as inp:
        c = pickle.load(inp)
        t = pickle.load(inp)
except IOError:
    print("Model does not exists")


class Predictor:
    def __init__(self, text):
        self.text = text
        self.result = None

    def prediction(self):
        self.result = predict(self.text, c, t)[0][0]
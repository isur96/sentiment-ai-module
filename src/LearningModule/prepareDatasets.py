import pandas as pd
from tqdm import tqdm
from src.paths import DATA_PATH, TOKENIZED_PATH
from src.Common.TextPrepare import tokenize_text
from .TextAnalyzer import TextAnalyzer

tqdm.pandas()


def get_sent(text):
    t = TextAnalyzer(text)
    t.analyze()
    return t.sentiment


dataset = pd.read_csv(filepath_or_buffer=DATA_PATH, delimiter=",", encoding="ISO-8859-1", header=None)
dataset.columns = ['target', 'ids', 'date', 'flag', 'user', 'text']
dataset.drop(['ids', 'date', 'flag', 'user'], axis=1, inplace=True)

print("Tokenization")
dataset['token'] = dataset.progress_apply(lambda row: tokenize_text(row['text']), axis=1)
print("Getting sentiments")
dataset['sent'] = dataset.progress_apply(lambda row: get_sent(row['text']), axis=1)
dataset.to_csv(TOKENIZED_PATH, index=False)

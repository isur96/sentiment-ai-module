from textblob import TextBlob
import re  # regex


class TextAnalyzer:
    def __init__(self, text) -> None:
        self.text = text
        self.polarity = None
        self.subjectivity = None
        self.sentiment = None
        self.subject = None

    def clear_text(self):
        text = self.text
        text = re.sub('@[A-Za-z0-9]+', '', text)  # remove @mentions
        text = re.sub('#', '', text)  # remove tags '#'
        text = re.sub('RT[\s]+', '', text)  # remove RT
        text = re.sub('https?:\/\/\S+', '', text)  # Remove hyperlinks
        self.text = text

    def text_polarity(self):
        self.polarity = TextBlob(self.text).sentiment.polarity

    def text_subjectivity(self):
        self.subjectivity = TextBlob(self.text).sentiment.subjectivity

    def get_sentiment(self):
        if self.polarity is None: self.text_polarity()

        if self.polarity > 0:
            self.sentiment = 1
        elif self.polarity < 0:
            self.sentiment = -1
        else:
            self.sentiment = 0

    def get_subject(self):
        if self.subjectivity is None: self.text_subjectivity()

        if self.subjectivity > 0:
            self.subject = 1
        elif self.subjectivity < 0:
            self.subject = -1
        else:
            self.subject = 0

    def analyze(self):
        self.text_subjectivity()
        self.text_polarity()
        self.get_sentiment()
        self.get_subject()
